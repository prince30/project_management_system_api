package inc.abc.project.management.system.restful.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import inc.abc.project.management.system.restful.api.dto.TaskDto;
import inc.abc.project.management.system.restful.api.entity.Project;
import inc.abc.project.management.system.restful.api.entity.Task;
import inc.abc.project.management.system.restful.api.repository.ProjectRepository;
import inc.abc.project.management.system.restful.api.repository.TaskRepository;
import inc.abc.project.management.system.restful.api.service.TaskService;
import inc.abc.project.management.system.restful.api.util.UniqueId;

@Service
public class TaskServiceImpl implements TaskService {

	private TaskRepository taskRepository;
	private ProjectRepository projectRepository;
	private UniqueId uniqueId;

	public TaskServiceImpl(TaskRepository taskRepository, UniqueId uniqueId, ProjectRepository projectRepository) {
		this.taskRepository = taskRepository;
		this.uniqueId = uniqueId;
		this.projectRepository = projectRepository;
	}

	@Override
	public TaskDto create(TaskDto taskDto) {
		Task taskEntity = new Task();
		BeanUtils.copyProperties(taskDto, taskEntity);

		Project projectEntity = projectRepository.findById(taskDto.getProjectId()).get();
		String publicTaskId = uniqueId.generateUniqueId(30);
		taskEntity.setTaskId(publicTaskId);
		taskEntity.setName(taskDto.getName());
		taskEntity.setStatus(taskDto.getStatus());
		taskEntity.setProject(projectEntity);

		Task taskStore = taskRepository.save(taskEntity);

		TaskDto returnValue = new TaskDto();
		BeanUtils.copyProperties(taskStore, returnValue);

		return returnValue;

	}

	@Override
	public TaskDto updateTask(String taskId, TaskDto taskDto) {
		TaskDto returnValue = new TaskDto();

		Task taskEntity = taskRepository.findByTaskId(taskId);
		Project projectEntity = projectRepository.findById(taskDto.getProjectId()).get();

		taskEntity.setName(taskDto.getName());
		taskEntity.setStatus(taskDto.getStatus());
		taskEntity.setProject(projectEntity);

		Task updateTask = taskRepository.save(taskEntity);

		BeanUtils.copyProperties(updateTask, returnValue);

		return returnValue;
	}

	@Override
	public void deleteTask(String taskId) {
		Task task = taskRepository.findByTaskId(taskId);

		taskRepository.delete(task);
	}

	@Override
	public List<TaskDto> fetchTasksByProjectId(String projectId, int page, int limit) {
		List<TaskDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Project projectEntity = projectRepository.findByProjectId(projectId);
		Page<Task> taskPage = taskRepository.findByProjectId(projectEntity.getId(), pageableRequest);
		List<Task> tasks = taskPage.getContent();

		for (Task task : tasks) {
			TaskDto taskDto = new TaskDto();
			BeanUtils.copyProperties(task, taskDto);
			returnValue.add(taskDto);
		}

		return returnValue;
	}

	@Override
	public TaskDto fetchTaskById(String taskId) {
		TaskDto returnValue = new TaskDto();

		Task taskEntity = taskRepository.findByTaskId(taskId);

		if (taskEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(taskEntity, returnValue);

		return returnValue;
	}

}

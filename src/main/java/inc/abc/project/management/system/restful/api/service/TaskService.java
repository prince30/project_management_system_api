package inc.abc.project.management.system.restful.api.service;

import java.util.List;
import inc.abc.project.management.system.restful.api.dto.TaskDto;

public interface TaskService {

	TaskDto create(TaskDto taskDto);

	TaskDto updateTask(String taskId, TaskDto taskDto);

	void deleteTask(String taskId);

	List<TaskDto> fetchTasksByProjectId(String projectId, int page, int limit);

	TaskDto fetchTaskById(String taskId);

}

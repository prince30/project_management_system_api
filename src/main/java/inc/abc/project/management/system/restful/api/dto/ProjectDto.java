package inc.abc.project.management.system.restful.api.dto;

import java.util.List;
import inc.abc.project.management.system.restful.api.util.Status;

public class ProjectDto {

	private String projectId;
	private String name;
	private String description;
	private Status status;
	private List<TaskDto> tasks;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<TaskDto> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskDto> tasks) {
		this.tasks = tasks;
	}

}

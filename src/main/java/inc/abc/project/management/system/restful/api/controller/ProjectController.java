package inc.abc.project.management.system.restful.api.controller;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import inc.abc.project.management.system.restful.api.dto.ProjectDto;
import inc.abc.project.management.system.restful.api.dto.TaskDto;
import inc.abc.project.management.system.restful.api.request.ProjectRequestData;
import inc.abc.project.management.system.restful.api.response.ErrorMessages;
import inc.abc.project.management.system.restful.api.response.OperationStatusModel;
import inc.abc.project.management.system.restful.api.response.ProjectResponse;
import inc.abc.project.management.system.restful.api.response.RequestOperationStatus;
import inc.abc.project.management.system.restful.api.response.TaskResponse;
import inc.abc.project.management.system.restful.api.service.ProjectService;
import inc.abc.project.management.system.restful.api.service.TaskService;

@RestController
@RequestMapping("projects") // http://localhost:9000/project-management-system/api/v1/projects
public class ProjectController {

	private ProjectService projectService;
	private TaskService taskService;

	public ProjectController(ProjectService projectService, TaskService taskService) {
		this.projectService = projectService;
		this.taskService = taskService;
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ProjectResponse store(@RequestBody ProjectRequestData projectRequestData) throws Exception {

		if (projectRequestData.getName().isEmpty() || projectRequestData.getDescription().isEmpty())
			throw new Exception(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		ProjectResponse returnValue = new ProjectResponse();

		ModelMapper modelMapper = new ModelMapper();
		ProjectDto projectDto = modelMapper.map(projectRequestData, ProjectDto.class);

		ProjectDto createdProject = projectService.create(projectDto);

		returnValue = modelMapper.map(createdProject, ProjectResponse.class);

		return returnValue;
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ProjectResponse fetchProjectById(@PathVariable String id) throws Exception {

		ProjectResponse returnValue = new ProjectResponse();

		ProjectDto projectDto = projectService.fetchProjectById(id);

		if (projectDto.getProjectId().isEmpty())
			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		BeanUtils.copyProperties(projectDto, returnValue);

		return returnValue;
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ProjectResponse update(@PathVariable String id, @RequestBody ProjectRequestData projectRequestData)
			throws Exception {
		if (projectRequestData.getName().isEmpty() || projectRequestData.getDescription().isEmpty())
			throw new Exception(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		ProjectResponse returnValue = new ProjectResponse();

		ProjectDto projectDto = new ProjectDto();

		BeanUtils.copyProperties(projectRequestData, projectDto);

		ProjectDto existId = projectService.fetchProjectById(id);

		if (existId.getProjectId().isEmpty())
			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		ProjectDto updateProject = projectService.updateProject(id, projectDto);

		BeanUtils.copyProperties(updateProject, returnValue);

		return returnValue;
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel delete(@PathVariable String id) throws Exception {
		ProjectDto projectDto = projectService.fetchProjectById(id);

		if (projectDto.getProjectId().isEmpty())
			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		projectService.deleteProject(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return returnValue;
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public List<ProjectResponse> fetchProjects(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<ProjectResponse> returnValue = new ArrayList<>();

		List<ProjectDto> projects = projectService.fetchProjects(page, limit);

		for (ProjectDto projectDto : projects) {
			ProjectResponse projectResponseModel = new ProjectResponse();
			BeanUtils.copyProperties(projectDto, projectResponseModel);
			returnValue.add(projectResponseModel);
		}

		return returnValue;
	}

	@GetMapping(path = "/{id}/tasks", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public List<TaskResponse> fetchTasksByProjectId(@PathVariable String id,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<TaskResponse> returnValue = new ArrayList<>();

		List<TaskDto> tasksDto = taskService.fetchTasksByProjectId(id, page, limit);

		if (tasksDto != null && !tasksDto.isEmpty()) {
			java.lang.reflect.Type listType = new TypeToken<List<TaskResponse>>() {
			}.getType();
			returnValue = new ModelMapper().map(tasksDto, listType);
		}
		return returnValue;

	}
}

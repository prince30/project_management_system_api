package inc.abc.project.management.system.restful.api.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import inc.abc.project.management.system.restful.api.util.Status;

@Entity
@Table(name = "projects")
public class Project extends AbstractEntity {

	@NotNull
	private String projectId;
	private String name;
	@Column(columnDefinition = "text")
	@NotNull(message = "Description Field is required.")
	private String description;
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Status Field is required.")
	private Status status;

	@OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
	private List<Task> tasks;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

}

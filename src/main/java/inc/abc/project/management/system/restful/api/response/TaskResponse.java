package inc.abc.project.management.system.restful.api.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import inc.abc.project.management.system.restful.api.util.Status;

public class TaskResponse {

	private String taskId;
	private String name;
	private Status status;
	private String projectId;

	@JsonIgnore
	public ProjectResponse project;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
}

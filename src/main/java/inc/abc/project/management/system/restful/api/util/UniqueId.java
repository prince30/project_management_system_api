package inc.abc.project.management.system.restful.api.util;

import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class UniqueId {

	public String generateUniqueId(int length) {
		return "UUID-" + UUID.randomUUID().toString().substring(length).toUpperCase();
	}
}

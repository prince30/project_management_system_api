package inc.abc.project.management.system.restful.api.request;

import java.util.List;
import inc.abc.project.management.system.restful.api.util.Status;

public class ProjectRequestData {

	private String name;
	private String description;
	private Status status;
	private List<TaskRequestData> tasks;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<TaskRequestData> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskRequestData> tasks) {
		this.tasks = tasks;
	}
}

package inc.abc.project.management.system.restful.api.util;

public enum Status {
	CREATED("Created"), 
	CANCELLED("Cancelled"), 
	INPROGRESS("InProgress"), 
	COMPLETED("Completed"), 
	ONHOLD("OnHold");

	private String code;

	private Status(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}

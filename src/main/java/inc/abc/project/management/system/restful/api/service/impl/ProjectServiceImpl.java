package inc.abc.project.management.system.restful.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import inc.abc.project.management.system.restful.api.dto.ProjectDto;
import inc.abc.project.management.system.restful.api.entity.Project;
import inc.abc.project.management.system.restful.api.repository.ProjectRepository;
import inc.abc.project.management.system.restful.api.service.ProjectService;
import inc.abc.project.management.system.restful.api.util.UniqueId;

@Service
public class ProjectServiceImpl implements ProjectService {

	private ProjectRepository projectRepository;
	private UniqueId uniqueId;

	public ProjectServiceImpl(ProjectRepository projectRepository, UniqueId uniqueId) {
		this.projectRepository = projectRepository;
		this.uniqueId = uniqueId;
	}

	@Override
	public ProjectDto create(ProjectDto projectDto) {
		ModelMapper modelMapper = new ModelMapper();
		Project projectEntity = modelMapper.map(projectDto, Project.class);

		String publicProjectId = uniqueId.generateUniqueId(30);
		projectEntity.setProjectId(publicProjectId);
		projectEntity.setName(projectDto.getName());
		projectEntity.setDescription(projectDto.getDescription());
		projectEntity.setStatus(projectDto.getStatus());

		Project projectStore = projectRepository.save(projectEntity);
		ProjectDto returnValue = modelMapper.map(projectStore, ProjectDto.class);

		return returnValue;
	}

	@Override
	public ProjectDto fetchProjectById(String projectId) {
		ProjectDto returnValue = new ProjectDto();

		Project projectEntity = projectRepository.findByProjectId(projectId);

		if (projectEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(projectEntity, returnValue);

		return returnValue;
	}

	@Override
	public ProjectDto updateProject(String projectId, ProjectDto projectDto) {
		ProjectDto returnValue = new ProjectDto();

		Project projectEntity = projectRepository.findByProjectId(projectId);

		projectEntity.setName(projectDto.getName());
		projectEntity.setDescription(projectDto.getDescription());
		projectEntity.setStatus(projectDto.getStatus());

		Project updateProject = projectRepository.save(projectEntity);

		BeanUtils.copyProperties(updateProject, returnValue);

		return returnValue;
	}

	@Override
	public void deleteProject(String projectId) {
		Project projectEntity = projectRepository.findByProjectId(projectId);

		projectRepository.delete(projectEntity);
	}

	@Override
	public List<ProjectDto> fetchProjects(int page, int limit) {
		List<ProjectDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Project> projectPage = projectRepository.findAll(pageableRequest);
		List<Project> projects = projectPage.getContent();

		for (Project project : projects) {
			ProjectDto projectDto = new ProjectDto();

			BeanUtils.copyProperties(project, projectDto);
			returnValue.add(projectDto);
		}

		return returnValue;
	}

}

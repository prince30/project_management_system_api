package inc.abc.project.management.system.restful.api.service;

import java.util.List;

import inc.abc.project.management.system.restful.api.dto.ProjectDto;

public interface ProjectService {

	ProjectDto create(ProjectDto projectDto);

	ProjectDto fetchProjectById(String projectId);

	ProjectDto updateProject(String projectId, ProjectDto projectDto);
	
	void deleteProject(String projectId);

	List<ProjectDto> fetchProjects(int page, int limit);

}

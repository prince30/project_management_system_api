package inc.abc.project.management.system.restful.api.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import inc.abc.project.management.system.restful.api.dto.TaskDto;
import inc.abc.project.management.system.restful.api.request.TaskRequestData;
import inc.abc.project.management.system.restful.api.response.ErrorMessages;
import inc.abc.project.management.system.restful.api.response.OperationStatusModel;
import inc.abc.project.management.system.restful.api.response.RequestOperationStatus;
import inc.abc.project.management.system.restful.api.response.TaskResponse;
import inc.abc.project.management.system.restful.api.service.TaskService;

@RestController
@RequestMapping("tasks") // http://localhost:9000/Task-management-system/api/v1/tasks
public class TaskController {

	private TaskService taskService;

	public TaskController(TaskService taskService) {
		this.taskService = taskService;
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public TaskResponse store(@RequestBody TaskRequestData taskRequestData) throws Exception {

		if (taskRequestData.getName().isEmpty())
			throw new Exception(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		TaskResponse returnValue = new TaskResponse();

		TaskDto taskDto = new TaskDto();

		BeanUtils.copyProperties(taskRequestData, taskDto);
		TaskDto createdTask = taskService.create(taskDto);

		BeanUtils.copyProperties(createdTask, returnValue);

		return returnValue;
	}

	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public TaskResponse update(@PathVariable String id, @RequestBody TaskRequestData taskRequestData) throws Exception {

		if (taskRequestData.getName().isEmpty())
			throw new Exception(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		TaskResponse returnValue = new TaskResponse();

		TaskDto taskDto = new TaskDto();

		BeanUtils.copyProperties(taskRequestData, taskDto);

		TaskDto existId = taskService.fetchTaskById(id);

		if (existId.getTaskId().isEmpty())
			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		TaskDto updateTask = taskService.updateTask(id, taskDto);

		BeanUtils.copyProperties(updateTask, returnValue);

		return returnValue;
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public OperationStatusModel delete(@PathVariable String id) throws Exception {
		TaskDto taskDto = taskService.fetchTaskById(id);

		if (taskDto.getTaskId().isEmpty())
			throw new Exception(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		taskService.deleteTask(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return returnValue;
	}
}

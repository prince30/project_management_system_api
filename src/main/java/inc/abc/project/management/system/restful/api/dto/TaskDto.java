package inc.abc.project.management.system.restful.api.dto;

import inc.abc.project.management.system.restful.api.util.Status;

public class TaskDto {
	private String taskId;
	private String name;
	private Status status;
	private Long projectId;
	// private ProjectDto project;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
}

package inc.abc.project.management.system.restful.api.response;

import inc.abc.project.management.system.restful.api.util.Status;

public class ProjectResponse {

	private String projectId;
	private String name;
	private String description;
	private Status status;
	// public List<TaskResponse> tasks = new ArrayList();

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}

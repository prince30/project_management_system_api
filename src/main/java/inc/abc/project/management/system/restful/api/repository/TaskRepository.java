package inc.abc.project.management.system.restful.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import inc.abc.project.management.system.restful.api.entity.Task;

@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

	Task findByTaskId(String taskId);

	Page<Task> findByProjectId(Long projectId, Pageable pageable);

}

package inc.abc.project.management.system.restful.api.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import inc.abc.project.management.system.restful.api.entity.Project;

@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

	Project findByProjectId(String projectId);

	Optional<Project> findById(Long id);

}
